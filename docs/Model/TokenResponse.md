# # TokenResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **string** |  | [optional]
**tokenType** | **string** |  | [optional]
**expiresIn** | **int** |  | [optional]
**refreshToken** | **string** |  | [optional]
**userName** | **string** |  | [optional]
**issued** | **string** |  | [optional]
**expires** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # UserProfile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **int** |  | [optional]
**firstName** | **string** |  | [optional]
**lastName** | **string** |  | [optional]
**nickName** | **string** |  | [optional]
**gender** | **string** |  | [optional]
**email** | **string** |  | [optional]
**countryCode** | **string** |  | [optional]
**mobile** | **string** |  | [optional]
**dateOfBirth** | **string** |  | [optional]
**areaId** | **int** |  | [optional]
**areaName** | **string** |  | [optional]
**areaCountry** | **string** |  | [optional]
**nationalityId** | **int** |  | [optional]
**notionalityName** | **string** |  | [optional]
**languageId** | **int** |  | [optional]
**languageName** | **string** |  | [optional]
**locationIde** | **int** |  | [optional]
**locationName** | **string** |  | [optional]
**marketingSourceId** | **int** |  | [optional]
**marketingSourceName** | **string** |  | [optional]
**customerSourceId** | **int** |  | [optional]
**customerSourceName** | **string** |  | [optional]
**hostId** | **int** |  | [optional]
**hostName** | **string** |  | [optional]
**marketingConsent** | **bool** |  | [optional]
**membershipNo** | **string** |  | [optional]
**memberCurrentStatus** | **string** |  | [optional]
**profileImagePath** | **string** |  | [optional]
**planDescription** | **string** |  | [optional]
**homeClubId** | **int** |  | [optional]
**homeClubName** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

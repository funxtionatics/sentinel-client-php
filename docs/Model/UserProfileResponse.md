# # UserProfileResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **bool** |  | [optional]
**message** | **string** |  | [optional]
**result** | [**\Funxtion\Integration\Sentinel\Api\Client\Model\UserProfile**](UserProfile.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

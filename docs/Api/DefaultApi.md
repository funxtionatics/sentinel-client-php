# Funxtion\Integration\Sentinel\Api\Client\DefaultApi

All URIs are relative to https://scopefitness.scopewebstore.com:5241.

Method | HTTP request | Description
------------- | ------------- | -------------
[**tokenPost()**](DefaultApi.md#tokenPost) | **POST** /Token | get a token


## `tokenPost()`

```php
tokenPost($grantType, $username, $password): \Funxtion\Integration\Sentinel\Api\Client\Model\TokenResponse
```

get a token

get a token

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new Funxtion\Integration\Sentinel\Api\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$grantType = 'grantType_example'; // string | the grant type in our case will always be password
$username = 'username_example'; // string | the username
$password = 'password_example'; // string | the password for the username

try {
    $result = $apiInstance->tokenPost($grantType, $username, $password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->tokenPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grantType** | **string**| the grant type in our case will always be password | [optional]
 **username** | **string**| the username | [optional]
 **password** | **string**| the password for the username | [optional]

### Return type

[**\Funxtion\Integration\Sentinel\Api\Client\Model\TokenResponse**](../Model/TokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

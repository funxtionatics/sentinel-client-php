# Funxtion\Integration\Sentinel\Api\Client\UserProfileApi

All URIs are relative to https://scopefitness.scopewebstore.com:5241.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUserProfile()**](UserProfileApi.md#getUserProfile) | **POST** /api/Sentinel/GetUserProfileByUsername | get the user profile


## `getUserProfile()`

```php
getUserProfile($userProfileByUsernameParameters): \Funxtion\Integration\Sentinel\Api\Client\Model\UserProfileResponse
```

get the user profile

get user profile by username

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new Funxtion\Integration\Sentinel\Api\Client\Api\UserProfileApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$userProfileByUsernameParameters = new \Funxtion\Integration\Sentinel\Api\Client\Model\UserProfileByUsernameParameters(); // \Funxtion\Integration\Sentinel\Api\Client\Model\UserProfileByUsernameParameters | Get a user profile by username

try {
    $result = $apiInstance->getUserProfile($userProfileByUsernameParameters);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserProfileApi->getUserProfile: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userProfileByUsernameParameters** | [**\Funxtion\Integration\Sentinel\Api\Client\Model\UserProfileByUsernameParameters**](../Model/UserProfileByUsernameParameters.md)| Get a user profile by username |

### Return type

[**\Funxtion\Integration\Sentinel\Api\Client\Model\UserProfileResponse**](../Model/UserProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

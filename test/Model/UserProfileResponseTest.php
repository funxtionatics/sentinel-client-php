<?php
/**
 * UserProfileResponseTest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  Funxtion\Integration\Sentinel\Api\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Sentinel API
 *
 * Just login for now
 *
 * The version of the OpenAPI document: draft
 * Contact: development@funxtion.com
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Funxtion\Integration\Sentinel\Api\Client\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * UserProfileResponseTest Class Doc Comment
 *
 * @category    Class
 * @description UserProfileResponse
 * @package     Funxtion\Integration\Sentinel\Api\Client
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class UserProfileResponseTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "UserProfileResponse"
     */
    public function testUserProfileResponse()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "success"
     */
    public function testPropertySuccess()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "message"
     */
    public function testPropertyMessage()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "result"
     */
    public function testPropertyResult()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
